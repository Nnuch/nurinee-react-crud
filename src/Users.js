import React, { useState, useEffect } from "react";
import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import IconButton from '@mui/material/IconButton';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import Typography from '@mui/material/Typography';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Collapse from '@mui/material/Collapse';
import Rating from '@mui/material/Rating';
import Stack from '@mui/material/Stack';
import { Container } from '@mui/material';
import './Users.css';


export default function BasicGrid() {
  const [users, setUsers] = useState([])
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  const ExpandMore = styled((props) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
  })(({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  }));;

  useEffect(() => {
    fetch("https://nurinee-node-crud.herokuapp.com/travel")
      .then(res => res.json())
      .then(
        (result) => {
          setUsers(result);
        }
      )
  }, [])


  return (
    <Container maxWidth="lg" sx={{ mt: 2 }}>
      <h1>
        นั่งรถไฟชิลๆ! เที่ยวในวันหยุด
      </h1>
      <Grid container spacing={2}>
        {users.map(user => (
          <Grid item xs={12} md={4}>
            <Card sx={{ maxWidth: 450 }} > 
              <CardHeader
              
                action={
                  <IconButton aria-label="settings">
                    <MoreVertIcon />
                  </IconButton>
                }
                title={user.id + '.' + user.name}
                subheader={'เปิดบริการ: ' + user.time}
              />
              
              <CardMedia
                component="img"
                height="250"
                image={user.avatar}
                alt={user.name}
              />
              <Stack spacing={1}>
                <Rating  name="size-medium" defaultValue={5} />
              </Stack>
              
              <CardActions disableSpacing>
                <IconButton aria-label="add to favorites">
                  <FavoriteIcon />
                </IconButton>
                <IconButton aria-label="share">
                  <ShareIcon />
                </IconButton>
                <ExpandMore
                  expand={expanded}
                  onClick={handleExpandClick}
                  aria-expanded={expanded}
                  aria-label="show more"
                >
                  <ExpandMoreIcon />
                </ExpandMore>
              </CardActions>

                <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent>
                    <Typography paragraph>อัตราค่าบริการ:</Typography>
                    <Typography paragraph color="green">
                      {' - ' + user.type}
                    </Typography>
                    <Typography paragraph color="green">
                      {' - ' + user.price}  
                    </Typography>
                  </CardContent>
                </Collapse>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
    
  );
}

